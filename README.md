# XSL-FO PDF Example

## Prerequisites

To generate the PDF from the XSL-FO file, we need to install [Apache FOP](https://xmlgraphics.apache.org/fop/).

### Debian/Ubuntu

```sh
sudo install fop
```

## Usage

```sh
fop doc.fo doc.pdf
```
